#!/bin/bash

password=$1
flag=0
len="${#password}"

green='\033[0;32m'
red='\033[0;31m'
clear='\033[0m'

if test $len -lt 10 ; then
    echo -e "${red}password length should be greater than or equal 10 hence weak password${clear}"
    flag=1
fi
echo "$password" | grep -q [0-9]

    if test $? -ne 0 ; then
        echo -e "${red}please include the numbers in password it is weak password${clear}"  
        flag=1
fi
echo "$password" | grep -q [A-Z]

    if test $? -ne 0 ; then
        echo -e "${red}weak password include capital char${clear}" 
        flag=1
fi
echo "$password" | grep -q [a-z]
    if test $? -ne 0 ; then 
        echo -e "${red}weak password include lower case char${clear}"
        flag=1
fi
if test $flag -eq 0 ; then 
     echo -e "${green} $password ${clear}Strong password"
fi
    

exit $flag